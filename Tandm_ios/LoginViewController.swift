//
//  LoginViewController.swift
//  Tandm_ios
//
//  Created by User on 5/5/18.
//  Copyright © 2018 User. All rights reserved.
//

import UIKit
import FBSDKLoginKit
import Firebase


class LoginViewController: UIViewController, FBSDKLoginButtonDelegate {
    func loginButton(_ loginButton: FBSDKLoginButton!, didCompleteWith result: FBSDKLoginManagerLoginResult!, error: Error!) {
        if(error != nil){
            print(error)
        }
        else {
            let credential = FacebookAuthProvider.credential(withAccessToken: FBSDKAccessToken.current().tokenString)
            Auth.auth().signIn(with: credential) { (user, error) in
                if let error = error {
                    print(error)
                    return
                }
                // User is signed in
            }
                self.showTandmView()
            print("logged in")
        }
    }
    func showTandmView() {
        present(ViewController(), animated: true, completion: nil)
    }
    
    
    func loginButtonDidLogOut(_ loginButton: FBSDKLoginButton!) {
        print("did log out")
    }
    
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        print("started")
        let loginButton = FBSDKLoginButton()
        view.addSubview(loginButton)
        loginButton.frame = CGRect(x: 16, y: 50, width: view.frame.width - 32, height: 50)
        loginButton.delegate = self
    }
    
}
