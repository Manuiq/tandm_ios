//
//  ViewController.swift
//  Tandm_ios
//
//  Created by User on 5/4/18.
//  Copyright © 2018 User. All rights reserved.
//

import UIKit

class ViewController: UIViewController{
    
    var overlay: UIView = UIView()
    
    var radius : CGFloat = 10
    
    var timer = Timer()
    
    var lastPosition : CGPoint? = nil

    
    func startIncreasingPlaground(){
        timer = Timer.scheduledTimer(withTimeInterval: 0.01, repeats: true) { (timer) in
            self.radius = self.radius + 2
            self.newMask(lastPosition: self.lastPosition!, radius: self.radius)
        }
    }
    func startDecreasingPlaground(){
        timer = Timer.scheduledTimer(withTimeInterval: 0.01, repeats: true) { (timer) in
            self.radius = self.radius - 2
            if(self.radius >= 0){
                self.newMask(lastPosition: self.lastPosition!,  radius: self.radius)
            }else{
                timer.invalidate()
            }
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        overlay = UIView(frame: CGRect(x: 0,y: 0,
                                       width: UIScreen.main.bounds.width,
                                       height: UIScreen.main.bounds.height))
        
    }
    
    private func newMask(lastPosition:CGPoint, radius: CGFloat) {
        
        // Set a semi-transparent, black background.
        overlay.backgroundColor = UIColor(red: 50, green: 50, blue: 0, alpha: 1)
        
        // Create the initial layer from the view bounds.
        let maskLayer = CAShapeLayer()
        maskLayer.frame = overlay.bounds
        maskLayer.fillColor = UIColor.blue.cgColor
        
        let rect = CGRect(
            x: lastPosition.x - radius,
            y: lastPosition.y - radius,
            width: 2 * radius,
            height: 2 * radius)
        
        // Create the path.
        let path = UIBezierPath(rect: overlay.bounds)
        maskLayer.fillRule = kCAFillRuleEvenOdd
        
        // Append the circle to the path so that it is subtracted.
        path.append(UIBezierPath(ovalIn: rect))
        maskLayer.path = path.cgPath
        
        // Set the mask of the view.
        overlay.layer.mask = maskLayer
        
        // Add the view so it is visible.
        self.view.addSubview(overlay)
        
    }
    
    func slideX(view: UIView, xBy: CGFloat, yBy: CGFloat) {
        let xPosition = view.frame.origin.x
        let yPosition = view.frame.origin.y
        
        let height = view.frame.height
        let width = view.frame.width
        
        UIView.animate(withDuration: 0.1, animations: {
            view.frame = CGRect(x: xPosition + xBy,
                                y: yPosition + yBy,
                                width: width,
                                height: height)
        })
        
    }
    
}

extension ViewController {
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesBegan(touches, with: event)
        guard let touchLocation = touches.first?.location(in: view) else { return }
        timer.invalidate()
        lastPosition = touchLocation
        startIncreasingPlaground()
    }
    
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesEnded(touches, with: event)
        timer.invalidate()
        startDecreasingPlaground()
    }
    
    override func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesMoved(touches, with: event)
        guard let touchLocation = touches.first?.location(in: view) else { return }
        lastPosition = touchLocation
        // update touch
    }
}


